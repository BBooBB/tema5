#include "..\..\..\include\spring\Application\ApplicationModel.h"
#include <spring\Application\InitialScene.h>
const std::string initialSceneName = "InitialSceneName";
namespace Spring
{
	ApplicationModel::ApplicationModel()
	{

	}
	void ApplicationModel::defineScene()
	{
		IScene *initialScene = new InitialScene(initialSceneName);
		m_Scenes.emplace(initialSceneName, initialScene);
	}
	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = initialSceneName;
	}
	void ApplicationModel::defineTransientData()
	{
		
	}
}
