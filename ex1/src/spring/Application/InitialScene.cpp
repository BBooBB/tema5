#include "..\..\..\include\spring\Application\InitialScene.h"

namespace Spring
{
	InitialScene::InitialScene(const std::string & ac_szSceneName):IScene(ac_szSceneName)
	{
	}
	void InitialScene::createScene()
	{
		createGUI();
	}
	void InitialScene::release()
	{
		delete centralWidget;
	}
	void InitialScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout = new QGridLayout(centralWidget);
		gridLayout->setSpacing(6);
		gridLayout->setContentsMargins(11, 11, 11, 11);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer_2, 1, 2, 1, 1);

		verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer, 2, 1, 1, 1);

		labelHelloWorld = new QLabel(centralWidget);
		labelHelloWorld->setObjectName(QStringLiteral("labelHelloWorld"));

		gridLayout->addWidget(labelHelloWorld, 1, 1, 1, 1);

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer, 1, 0, 1, 1);

		verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer_2, 0, 1, 1, 1);

		m_uMainWindow.get()->setCentralWidget(centralWidget);

		labelHelloWorld->setText("Hello world!");
	}
}
