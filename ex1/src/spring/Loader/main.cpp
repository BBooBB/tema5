#include<spring\Framework\Application.h>
#include <spring\Application\ApplicationModel.h>
#include <qapplication.h>

int main(int argc, char **argv)
{
	QApplication app(argc, argv);

	Spring::Application& application = Spring::Application::getInstance();
	Spring::ApplicationModel appModel;
	application.setApplicationModel(&appModel);

	application.start("Hello world!", 300, 300);

	app.exec();
	return 0;
}