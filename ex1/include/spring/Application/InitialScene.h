#pragma once
#include <spring\Framework\IScene.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
namespace Spring
{
	class InitialScene :public IScene
	{
		
	public:
		InitialScene(const std::string& ac_szSceneName);
		void createScene() override;
		void release() override;
	private:
		void createGUI();

		QWidget *centralWidget;
		QGridLayout *gridLayout;
		QSpacerItem *horizontalSpacer_2;
		QSpacerItem *verticalSpacer;
		QLabel *labelHelloWorld;
		QSpacerItem *horizontalSpacer;
		QSpacerItem *verticalSpacer_2;
		

	};
}
