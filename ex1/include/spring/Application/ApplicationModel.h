#pragma once
#include <spring\Framework\IApplicationModel.h>
#include <spring\Application\global.h>
namespace Spring
{
	class Application_EXPORT_IMPORT_API ApplicationModel:public IApplicationModel
	{
	public:
		ApplicationModel();
		void defineScene() override;

		void defineInitialScene() override;

		void defineTransientData() override;
	};
}
