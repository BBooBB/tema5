#pragma once
#include <spring\Framework\IScene.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

namespace Spring
{
	class InitialScene : public IScene
	{
		Q_OBJECT

	public:

		explicit InitialScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~InitialScene();

	private:
		void createGUI();

		QWidget *centralWidget;
		QGridLayout *gridLayout_2;
		QGridLayout *gridLayout;
		QLineEdit *lineEditNume;
		QPushButton *pushButtonSayHello;
		QSpacerItem *verticalSpacer_3;
		QSpacerItem *horizontalSpacer_2;
		QSpacerItem *horizontalSpacer;
		QSpacerItem *verticalSpacer;
		QSpacerItem *verticalSpacer_2;

		private slots:
		void sayHelloButton();

	};
}
