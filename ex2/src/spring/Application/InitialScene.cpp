#include "..\..\..\include\spring\Application\InitialScene.h"
#include <qapplication.h>
#include <iostream>

namespace Spring
{
	InitialScene::InitialScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void InitialScene::createScene()
	{
		createGUI();

		QObject::connect(pushButtonSayHello, SIGNAL(released()), this, SLOT(sayHelloButton()));
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	InitialScene::~InitialScene()
	{
	}

	void InitialScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout_2 = new QGridLayout(centralWidget);
		gridLayout_2->setSpacing(6);
		gridLayout_2->setContentsMargins(11, 11, 11, 11);
		gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(6);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		lineEditNume = new QLineEdit(centralWidget);
		lineEditNume->setObjectName(QStringLiteral("lineEditNume"));

		gridLayout->addWidget(lineEditNume, 0, 0, 1, 1);

		pushButtonSayHello = new QPushButton(centralWidget);
		pushButtonSayHello->setObjectName(QStringLiteral("pushButtonSayHello"));

		gridLayout->addWidget(pushButtonSayHello, 2, 0, 1, 1);

		verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer_3, 1, 0, 1, 1);


		gridLayout_2->addLayout(gridLayout, 1, 1, 1, 1);

		horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout_2->addItem(horizontalSpacer_2, 1, 2, 1, 1);

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout_2->addItem(horizontalSpacer, 1, 0, 1, 1);

		verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout_2->addItem(verticalSpacer, 0, 1, 1, 1);

		verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout_2->addItem(verticalSpacer_2, 2, 1, 1, 1);

		m_uMainWindow.get()->setCentralWidget(centralWidget);
		pushButtonSayHello->setText("SayHello!");
	}

	void InitialScene::sayHelloButton()
	{
		const std::string name = lineEditNume->text().toStdString();

		if (!name.empty())
		{
			m_TransientDataCollection.emplace("Name", name);
			const std::string c_szNextSceneName = "BaseScene";
			emit SceneChange(c_szNextSceneName);
		}

		
	}
}

