#include "..\..\..\include\spring\Application\BaseScene.h"

namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void BaseScene::createScene()
	{
		
		std::string title = "Scene2";
		m_uMainWindow->setWindowTitle(QString(title.c_str()));

		createGUI();

		QObject::connect(pushButtonBack, SIGNAL(released()), this, SLOT(backButton()));
	}

	void BaseScene::release()
	{
	}
	
	BaseScene::~BaseScene()
	{
	}
	void BaseScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout_2 = new QGridLayout(centralWidget);
		gridLayout_2->setSpacing(6);
		gridLayout_2->setContentsMargins(11, 11, 11, 11);
		gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(6);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		labelHello = new QLabel(centralWidget);
		labelHello->setObjectName(QStringLiteral("labelHello"));
		labelHello->setAlignment(Qt::AlignCenter);

		gridLayout->addWidget(labelHello, 0, 0, 1, 1);

		pushButtonBack = new QPushButton(centralWidget);
		pushButtonBack->setObjectName(QStringLiteral("pushButtonBack"));

		gridLayout->addWidget(pushButtonBack, 1, 0, 1, 1);


		gridLayout_2->addLayout(gridLayout, 1, 1, 1, 1);

		verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout_2->addItem(verticalSpacer, 2, 1, 1, 1);

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout_2->addItem(horizontalSpacer, 1, 0, 1, 1);

		verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout_2->addItem(verticalSpacer_2, 0, 1, 1, 1);

		horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout_2->addItem(horizontalSpacer_2, 1, 2, 1, 1);

		m_uMainWindow.get()->setCentralWidget(centralWidget);


		std::string name = "Hello "+boost::any_cast<std::string>(m_TransientDataCollection["Name"])+"!";

		labelHello->setText(QString(name.c_str()));
		pushButtonBack->setText("Back");
	}
	void BaseScene::backButton()
	{
		m_TransientDataCollection.erase("Name");

		const std::string c_szNextSceneName = "InitialScene";

		std::string title = "Scene1";
		m_uMainWindow->setWindowTitle(QString(title.c_str()));

		emit SceneChange(c_szNextSceneName);
	}
}
