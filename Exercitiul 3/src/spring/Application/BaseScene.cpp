#include "..\..\..\include\spring\Application\BaseScene.h"
#include <string>
namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void BaseScene::createScene()
	{	
		createGUI();

		QObject::connect(pushButtonNext, SIGNAL(released()), this, SLOT(mf_pushButtonNext()));
		QObject::connect(pushButtonPrevious, SIGNAL(released()), this, SLOT(mf_pushButtonPrevious()));

	}

	void BaseScene::release()
	{
		delete centralWidget;
	}
	
	BaseScene::~BaseScene()
	{
	}
	void BaseScene::createGUI()
	{

		m_uMainWindow.get()->resize(400, 300);
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout_2 = new QGridLayout(centralWidget);
		gridLayout_2->setSpacing(6);
		gridLayout_2->setContentsMargins(11, 11, 11, 11);
		gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(6);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer, 3, 0, 1, 1);

		lineEditIndex = new QLineEdit(centralWidget);
		lineEditIndex->setObjectName(QStringLiteral("lineEditIndex"));
		std::string index = std::to_string(boost::any_cast<int>(m_TransientDataCollection["sceneIndex"]));
		lineEditIndex->setText(QString(index.c_str()));
		lineEditIndex->setEnabled(false);

		gridLayout->addWidget(lineEditIndex, 2, 0, 1, 2);

		pushButtonPrevious = new QPushButton(centralWidget);
		pushButtonPrevious->setObjectName(QStringLiteral("pushButtonPrevious"));

		gridLayout->addWidget(pushButtonPrevious, 4, 0, 1, 1);

		pushButtonNext = new QPushButton(centralWidget);
		pushButtonNext->setObjectName(QStringLiteral("pushButtonNext"));

		gridLayout->addWidget(pushButtonNext, 4, 1, 1, 1);

		verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer_4, 3, 1, 1, 1);


		gridLayout_2->addLayout(gridLayout, 1, 1, 1, 1);

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout_2->addItem(horizontalSpacer, 1, 0, 1, 1);

		verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout_2->addItem(verticalSpacer_2, 0, 1, 1, 1);

		verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout_2->addItem(verticalSpacer_3, 2, 1, 1, 1);

		horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout_2->addItem(horizontalSpacer_2, 1, 2, 1, 1);

		m_uMainWindow.get()->setCentralWidget(centralWidget);

		pushButtonPrevious->setText("Previous");
		pushButtonNext->setText("Next");
	}
	void BaseScene::mf_pushButtonNext()
	{

		int currentIndex = boost::any_cast<int>(m_TransientDataCollection.find("sceneIndex")->second);

		m_TransientDataCollection.erase("sceneIndex");
		m_TransientDataCollection.emplace("sceneIndex", currentIndex + 1);
		
		const std::string c_szNextSceneName = "BaseScene";
		emit SceneChange(c_szNextSceneName);
	}
	void BaseScene::mf_pushButtonPrevious()
	{
		int currentIndex = boost::any_cast<int>(m_TransientDataCollection.find("sceneIndex")->second);
	
		m_TransientDataCollection.erase("sceneIndex");
		m_TransientDataCollection.emplace("sceneIndex", currentIndex - 1);

		if (currentIndex == 2)
		{
			const std::string c_szNextSceneName = "InitialScene";
			emit SceneChange(c_szNextSceneName);
		}
		else
		{
			const std::string c_szNextSceneName = "BaseScene";
			emit SceneChange(c_szNextSceneName);
		}
		
	}
}
