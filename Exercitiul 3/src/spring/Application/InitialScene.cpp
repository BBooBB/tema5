#include "..\..\..\include\spring\Application\InitialScene.h"
#include <qapplication.h>
#include <iostream>

namespace Spring
{
	InitialScene::InitialScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void InitialScene::createScene()
	{
		createGUI();
		QObject::connect(pushButtonNext, SIGNAL(released()), this, SLOT(mf_pushButtonNext()));
	
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	InitialScene::~InitialScene()
	{
	}

	void InitialScene::createGUI()
	{
		m_uMainWindow.get()->resize(400, 300);
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout_2 = new QGridLayout(centralWidget);
		gridLayout_2->setSpacing(6);
		gridLayout_2->setContentsMargins(11, 11, 11, 11);
		gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(6);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer, 3, 0, 1, 1);

		lineEditIndex = new QLineEdit(centralWidget);
		lineEditIndex->setObjectName(QStringLiteral("lineEditIndex"));
		lineEditIndex->setEnabled(false);
		lineEditIndex->setText("1");
		gridLayout->addWidget(lineEditIndex, 2, 0, 1, 2);

		pushButtonNext = new QPushButton(centralWidget);
		pushButtonNext->setObjectName(QStringLiteral("pushButtonNext"));

		gridLayout->addWidget(pushButtonNext, 4, 1, 1, 1);

		verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer_4, 3, 1, 1, 1);


		gridLayout_2->addLayout(gridLayout, 1, 1, 1, 1);

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout_2->addItem(horizontalSpacer, 1, 0, 1, 1);

		verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout_2->addItem(verticalSpacer_2, 0, 1, 1, 1);

		verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout_2->addItem(verticalSpacer_3, 2, 1, 1, 1);

		horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout_2->addItem(horizontalSpacer_2, 1, 2, 1, 1);

		m_uMainWindow.get()->setCentralWidget(centralWidget);

		pushButtonNext->setText("Next");
	}
	void InitialScene::mf_pushButtonNext()
	{
		m_TransientDataCollection.erase("sceneIndex");
		m_TransientDataCollection.emplace("sceneIndex", 2);
		const std::string c_szNextSceneName = "BaseScene";
		emit SceneChange(c_szNextSceneName);
		
		
	}
}

