#pragma once
#include <spring\Framework\IScene.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>
namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT
	public:

		explicit BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~BaseScene();
	private:
		QWidget *centralWidget;
		QGridLayout *gridLayout_2;
		QGridLayout *gridLayout;
		QSpacerItem *verticalSpacer;
		QLineEdit *lineEditIndex;
		QPushButton *pushButtonPrevious;
		QPushButton *pushButtonNext;
		QSpacerItem *verticalSpacer_4;
		QSpacerItem *horizontalSpacer;
		QSpacerItem *verticalSpacer_2;
		QSpacerItem *verticalSpacer_3;
		QSpacerItem *horizontalSpacer_2;
	private slots:
		void mf_pushButtonNext();
		void mf_pushButtonPrevious();
	private:
		void createGUI();
		
	};

}
